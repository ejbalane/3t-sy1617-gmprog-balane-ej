﻿using UnityEngine;
using System.Collections;

public class CameraFollower : MonoBehaviour {
	public Transform player;
	public Vector3 CameraOffset;
	public Vector3 CameraOrthozoom;
	private float zoomSpeed = 5.0f;
	//private Camera camera;
	private float scalex;
	//public float Add = 1.0f;
	// Use this for initialization
	void Start () {
		scalex = player.transform.localScale.x;
	}
	
	// Update is called once per frame
	void Update () {
		
		//Camera follows Player with additional offset
		transform.position = new Vector3 (player.position.x + CameraOffset.x, player.position.y + CameraOffset.y, CameraOffset.z);

		if (player.transform.localScale.x > scalex) {
			transform.position = Vector3.MoveTowards(transform.position, CameraOrthozoom, zoomSpeed * Time.deltaTime);
		}
//		if (player.transform.localScale++) {
//			Camera.main.orthographicSize++;
//		}
	}
}
