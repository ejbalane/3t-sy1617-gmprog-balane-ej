﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

public enum State{
	Eat,
	Run,
	Patrol,
}
public class EnemyScript : MonoBehaviour {
	//public string Tag;
	//public float Add;
	public State curState;
	//public GameObject nearbyEnemy;
	public Vector3 detectRadius = new Vector3(10,10,10);
	private Vector3 newTransform;

	public Rigidbody rb;
	public float size = 0;
	public float speed = 5;

	public GameObject player;

	private GameObject target;
	private GameObject closestPlayer;
	private GameObject closestEnemy;
	private GameObject closestFood;

	public float MinimumDistance = 20.0f;
//	void OnTriggerEnter(Collider other){
//		if (other.gameObject.tag == Tag) {
//			transform.localScale += new Vector3 (Add, Add, Add);
//			Destroy (other.gameObject);
//		}
//	}
	// Use this for initialization
	void Start () {
		//Initialize Player
		player = GameObject.FindGameObjectWithTag("Player");

		//Initialise Lists


		//Random Checkpoint
		newTransform.x = Random.Range(-50,50);
		newTransform.y = Random.Range (-50, 50);
		newTransform.z = 0;
		transform.position = newTransform;



		rb = GetComponent<Rigidbody> ();
	}
	
	// Update is called once per frame
	void Update () {


		closestFood = GameObject.FindGameObjectsWithTag ("Food").OrderBy (go => Vector3.Distance (go.transform.position, transform.position)).FirstOrDefault ();
		closestEnemy = GameObject.FindGameObjectsWithTag ("Enemy").OrderBy (go => Vector3.Distance (go.transform.position, transform.position)).FirstOrDefault ();
		closestPlayer = GameObject.FindGameObjectsWithTag ("Player").OrderBy (go => Vector3.Distance (go.transform.position, transform.position)).FirstOrDefault ();


		switch (curState) {
		case State.Run:
			UpdateRun ();
			break;
		case State.Eat:
			UpdateEat ();
			break;
		case State.Patrol:
			UpdatePatrol ();
			break;
		}	

	}

	void UpdateRun(){
		target = closestEnemy;
		if (closestEnemy.transform.localScale.x > transform.localScale.x) {
			curState = State.Run;

			target = closestFood;
			transform.position = Vector3.MoveTowards(transform.position, target.transform.position, speed * Time.deltaTime/transform.localScale.x);
		}
		if (target == null) {
			closestEnemy = GameObject.FindGameObjectsWithTag("Enemy").OrderBy(go => Vector3.Distance(go.transform.position, transform.position)).FirstOrDefault();

		}
		if (target == null) {

			curState = State.Patrol;
		}
	}

	void UpdateEat(){		
		target = closestEnemy;
		if (closestEnemy.transform.localScale.x < transform.localScale.x) {
			transform.position = Vector3.MoveTowards (transform.position, target.transform.position, speed * Time.deltaTime / transform.localScale.x);
		}
	}

	void UpdatePatrol(){
		
		target = closestFood;
		transform.position = Vector3.MoveTowards (transform.position, target.transform.position, speed * Time.deltaTime/transform.localScale.x);
		if (target == null) {
			closestFood = GameObject.FindGameObjectsWithTag ("Food").OrderBy (go => Vector3.Distance (go.transform.position, transform.position)).FirstOrDefault ();
			target = closestFood;
			transform.position = Vector3.MoveTowards (transform.position, target.transform.position, speed * Time.deltaTime/transform.localScale.x);
		}

	}


}
