﻿using UnityEngine;
using System.Collections;

public class FoodSpawner : MonoBehaviour {
	public GameObject Foodprefab;
//	public GameObject Enemyprefab;
	public float repeatrate;

	void GenerateFood(){
		int x = Random.Range (-50,50/*Camera.main.pixelWidth*/);
		int y = Random.Range (-50,50/*Camera.main.pixelHeight*/);

		Vector3 Target = Camera.main.ScreenToWorldPoint (new Vector3(x,y,0));
		Target.z = 0;

		Instantiate (Foodprefab, new Vector3 (Random.Range (-50,50), Random.Range (-50,50), 0), Quaternion.identity);
	}
	// Use this for initialization
	void Start () {
		for (int i = 0; i <= 30; i++) {
			Instantiate (Foodprefab, new Vector3 (Random.Range (-50, 50), Random.Range (-50,50), 0), Quaternion.identity);
		}
		InvokeRepeating ("GenerateFood", 0, repeatrate);
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
