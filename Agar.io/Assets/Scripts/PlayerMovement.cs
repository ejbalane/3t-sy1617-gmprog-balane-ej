﻿using UnityEngine;
using System.Collections;

public class PlayerMovement : MonoBehaviour {
	public GameObject Player;
	public Transform player;
	//constraints
	private float minimumX = -50f;
	private float maximumX = 50f;
	private float minimumY = -50;
	private float maximumY = 50f;

	public float PlayerSpeed = 5;
	//for next milestone, add boundaries using clamping (Mathf)
	//private float x;
	//private float y;
	// Use this for initialization
	void Start () {
		//x = player.transform.position.x;
		//y = player.transform.position.y;
	}
	
	// Update is called once per frame
	void Update () {
		Vector3 playerPosition = transform.position;
		playerPosition.x = Mathf.Clamp (playerPosition.x, minimumX, maximumX);
		playerPosition.y = Mathf.Clamp (playerPosition.y, minimumY, maximumY);

		Vector3 GotoPoint = Camera.main.ScreenToWorldPoint (Input.mousePosition);

		GotoPoint.z = playerPosition.z;

		transform.position = Vector3.MoveTowards (playerPosition, GotoPoint, PlayerSpeed * Time.deltaTime/transform.localScale.x);
	}

	//General movement Script
	void Movement(){
//		Vector3 GotoPoint = Camera.main.ScreenToWorldPoint (Input.mousePosition);
//
//		GotoPoint.z = transform.position.z;
//
//		transform.position = Vector3.MoveTowards (transform.position, GotoPoint, PlayerSpeed * Time.deltaTime/transform.localScale.x);

	}
}
