﻿using UnityEngine;
using System.Collections;

public class PMovement : MonoBehaviour {
	public float Speed = 5f;
	//constraints
	private float minimumX = -10f;
	private float maximumX = 10f;
	private float minimumY = -2f;
	private float maximumY = 6f;
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		Movement ();
		Vector3 playerPosition = transform.position;
		playerPosition.x = Mathf.Clamp (playerPosition.x, minimumX, maximumX);
		playerPosition.y = Mathf.Clamp (playerPosition.y, minimumY, maximumY);
		transform.position = playerPosition;
	}

	//General Movement Script
	void Movement(){
		if (Input.GetKey (KeyCode.A)) {
			transform.Translate (Vector3.left * Time.deltaTime * Speed);
		}
		if (Input.GetKey (KeyCode.D)) {
			transform.Translate (Vector3.right * Time.deltaTime * Speed);
		}
		if (Input.GetKey (KeyCode.W)) {
			transform.Translate (Vector3.up * Time.deltaTime * Speed);
		}	
		if (Input.GetKey (KeyCode.S)) {
			transform.Translate (Vector3.down * Time.deltaTime * Speed);
		}	
	}

	void OnCollisionEnter(Collision other){
		if (other.gameObject.name == "Rock(Clone)") {
			Object.Destroy (other.gameObject);
			Object.Destroy (this.gameObject);
			Debug.Log ("GAME OVER");
		}
	}
}
