﻿using UnityEngine;
using System.Collections;

public class RockSpawner : MonoBehaviour {
	public GameObject RockPrefab;
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	void Spawn(){
		GameObject rock = (GameObject)Object.Instantiate (RockPrefab, new Vector3 (Random.Range (-10, 10), 10f, 0f), Quaternion.identity);
		//Object.Destroy (rock, 5f);
	}
}
