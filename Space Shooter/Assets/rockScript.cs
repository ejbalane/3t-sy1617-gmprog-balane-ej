﻿using UnityEngine;
using System.Collections;

public class rockScript : MonoBehaviour {
	public float Speed = 5f;
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
		transform.Translate (Vector3.down * Time.deltaTime * Speed, Space.World);
	}
}
